package consumer

type RunError struct {
	msg string
}

func (e *RunError) Error() string {
	return e.msg
}
